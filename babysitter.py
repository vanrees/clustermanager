#!/usr/bin/env python3

"""
Given a list of yaml files, each representing a job for the cluster, check
their status. If they are done, call 'deciders' that will verify the results
and provide new jobs to submit. Different clusters (or other environments)
correspond to different 'worlds' defined in manyworlds.
"""

import argparse
import jsonlogger
import os.path
import sys
import yaml
# import the different deciders
from sdpbtools.sdpbanalyzer import sdpbcompletedDecider
from sdpbtools.sdpbanalyzer import nextinbinarysearchDecider
from sdpbtools.sdpbanalyzer import nextinsequencerDecider

deciders = {'sdpbcompleted': sdpbcompletedDecider,
            'nextinbinarysearch': nextinbinarysearchDecider,
            'nextinsequencer': nextinsequencerDecider}

parser = argparse.ArgumentParser()
parser.add_argument('filenames',
                    metavar='fn',
                    nargs='+',
                    help="""A number of yaml files to handle.""")
parser.add_argument('-w',
                    '--world',
                    choices=['local', 'cern'],
                    default='local',
                    help=("Select the environment for submitting or"
                          "running jobs, and for checking whether a"
                          "submitted job is really running."))
parser.add_argument('-m',
                    '--maxsubmissions',
                    type=int,
                    default=10,
                    help="""Maximum number of resubmissions for a job.""")
parser.add_argument('-r',
                    '--reallyrunning',
                    action='store_true',
                    help=("Query the cluster to see if job is really"
                          "running. Resubmit if this is not the case."))
parser.add_argument('-f',
                    '--force',
                    action='store_true',
                    help="""force resubmission of a failed job'""")
args = parser.parse_args()

# choose world: default is local
if args.world == 'local':
    import manyworlds.localworld
    world = manyworlds.localworld
elif args.world == 'cern':
    import manyworlds.cernworld
    world = manyworlds.cernworld
else:
    raise ValueError('Unknown world')


def nextinline(filename):
    """
    A function that takes the filename for a job that is done, verifies it
    completed successfully and returns the name of a new job if needed.
    Reads the various 'deciders' from the filename and executes them in the
    given order. Each decider returns (success, newfilename) and so does
    this function.
    """
    stream = open(filename, 'r')
    parse = yaml.safe_load(stream)

    if 'analyze' in parse:
        for decider in parse['analyze']:
            deciderfn = deciders[decider]
            success, newfilename = deciderfn(filename)
            if not success or newfilename is not None:
                return (success, newfilename)

    return (True, None)


def handle(filename):
    """
    Main function that 'handles' the filename and updates its status in the
    associated log file. It can submit a job or declare it done; in the latter
    because it calls nextinline(..) which in turn calls the decider functions
    that are listed in the file itself.
    """
    def inlineprint(expr):
        print(expr, end=' ', flush=True)

    logfilename = filename + '.log'
    log = jsonlogger.JSONlog('babysitter', logfilename)
    status = log.getstatus()

    if status == 'terminated':
        inlineprint('terminated.')
    elif status == 'tosubmit':
        log.setstatus('submitted')
        submissionresult = world.submit(filename)
        log.write({'submissionresult': submissionresult})
        handle(filename)
    elif status is None:
        inlineprint('submitting...')
        log.setstatus('tosubmit')
        handle(filename)
    elif status == 'submitted' or status == 'running':
        inlineprint('submitted.')
        if args.reallyrunning:
            inlineprint('checking...')
            submissionresult = log.getlastvalueof('submissionresult')
            if world.check(submissionresult):
                inlineprint('is really running.')
            else:
                inlineprint('is NOT really running:')
                log.setstatus('failed')
                handle(filename)
    elif status == 'done':
        success, newfilename = nextinline(filename)
        if not success:
            log.setstatus('failed')
            handle(filename)
        else:
            if newfilename is None:
                inlineprint('is done and terminated.')
                log.setstatus('terminated')
            elif newfilename == filename:
                inlineprint('is done; resubmitting...')
                log.setstatus('tosubmit')
                handle(filename)
            else:
                print('is done; replaced -->')
                inlineprint(os.path.basename(newfilename) + ' :')
                log.setstatus('terminated')
                handle(newfilename)
    elif status == 'failed':
        if args.force:
            inlineprint('failed; forced resubmission...')
            log.setstatus('tosubmit')
            handle(filename)
        else:
            inlineprint('failed.')
    else:
        # How did you get here?
        print('Unknown status for ' + filename + '!')


# check if all files exist
filenamelist = args.filenames
if not all(map(os.path.isfile, filenamelist)):
    print('Could not find sdp data file(s)', file=sys.stderr)
    exit(1)


for filename in filenamelist:
    print(os.path.basename(filename) + ' :', end=' ', flush=True)
    handle(filename)
    print()
