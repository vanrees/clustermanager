import json
from jsonschema import validate
import time
import os

statuses = ["tosubmit", "submitted", "running", "done", "terminated", "failed"]
schema = {
    "type" : "object",
    "required": ["timestamp", "origin"],
    "properties" : {
        "timestamp" : {"type" : "string"},
        "origin" : {"type" : "string"},
        "status" : {"type" : "string", "enum" : statuses},
    }
}

class JSONlog:
	"""
	Simple logger where every line should obey the above JSON schema. Only the 
	keyword "status" is protected. The "timestamp" and "origin" keys are added
	automatically.

	Basic tests: OK for all methods
	"""

	def __init__(self, whoisthis, filename):
		self.filename = os.path.abspath(filename)
		self.whoisthis = whoisthis
		if not os.path.isfile(filename):
			open(filename,'a').close()

	def write(self, message):
		fp = open(self.filename, 'a', 1)
		completemessage = {"timestamp" : time.asctime(time.localtime()), "origin": self.whoisthis}
		completemessage.update(message)
		validate(completemessage, schema=schema)
		fp.write(json.dumps(completemessage)+"\n")
		fp.close()

	def getvaluesof(self,key):
		fp = open(self.filename, 'r')
		values = []
		for line in fp:
			linedict = json.loads(line)
			if key in linedict:
				values.append(linedict[key])
		return values

	def getlastvalueof(self,key):
		if self.getvaluesof(key):
			return self.getvaluesof(key)[-1]

	def getlogitemswith(self,constraintdict):
		fp = open(self.filename, 'r')
		values = []
		for line in fp:
			linedict = json.loads(line)
			if constraintdict.items() <= linedict.items():
				values.append(linedict)
		return values

	def setstatus(self, status):
		self.write({"status" : status})

	def getstatus(self):
		return self.getlastvalueof("status")
