import os
import json
import sys
import decimal
from ruamel.yaml import YAML
sys.path.insert(0, '..')
import jsonlogger


def _update_aliased_scalar(data, parent, key_index, val):
    """
    workaround. See
    https://stackoverflow.com/questions/58118589/ruamel-yaml-recurse-function-
    unexpectedly-updates-every-instance-of-non-string
    for the issue.
    """
    def recurse(d, parent, key_index, ref, nv):
        if isinstance(d, dict):
            for i, k in [(idx, key) for idx, key
                         in enumerate(d.keys()) if key is ref]:
                d.insert(i, nv, d.pop(k))
            for k, v in d.non_merged_items():
                if v is ref:
                    if hasattr(v, 'anchor') or \
                       (d is parent and k == key_index):
                        d[k] = nv
                else:
                    recurse(v, parent, key_index, ref, nv)
        elif isinstance(d, list):
            for idx, item in enumerate(d):
                if item is ref:
                    d[idx] = nv
                else:
                    recurse(item, parent, key_index, ref, nv)

    obj = parent[key_index]
    if hasattr(obj, 'anchor'):
        recurse(data, parent, key_index, obj,
                type(obj)(val, anchor=obj.anchor.value))
    else:
        recurse(data, parent, key_index, obj, type(obj)(val))


def _getsdpbresults(filename):
    """
    A function that gets the sdpb results for the 'done' job. Its output
    should be a dictionary that captures the contents of 'out.txt' from
    sdpb.

    This function assumes that 'sdpoutput2json.py' has been run and its
    output is stored in the log file, so that is where we load the output
    from. This requires an execution like:

    - program: "sdpoutput2json.py"
      cmdlineparams:
      sdpDir : *sdpdir
      options:
      capture_output : true

    in every job file.
    """
    logfilename = filename + '.log'
    log = jsonlogger.JSONlog('sdpanalyzer', logfilename)
    logitem = log.getlogitemswith({'finished': 'sdpoutput2json.py'})
    try:
        # potential todo: add json schema for sdpb results.
        sdpbresults = json.loads(logitem[-1]['stdout'])
    except (IndexError, KeyError):
        raise ValueError('No sdpb result found.')
    except (json.decoder.JSONDecodeError):
        raise ValueError('Error parsing sdpb result as json.')
    return sdpbresults


def _counteradd(filename):
    """
    either adds a counter to a file:
        input:  file.ext
        output: file.count001.ext
    or adds one to counter of a file:
        input:  file.count015.ext
        output: file.count016.ext
    """
    filenameroot, filenamext = os.path.splitext(filename)
    filenamebare, maybecounter = os.path.splitext(filenameroot)
    if maybecounter[:6] == '.count':
        counter = '.count' + str(int(maybecounter[6:]) + 1).zfill(3)
        return filenamebare + counter + filenamext
    else:
        return filenamebare + maybecounter + '.count001' + filenamext


def sdpbcompletedDecider(filename):
    """
    Checks if sdpb completed successfully. Returns (success, newfilename).
    """
    sdpbresults = _getsdpbresults(filename)
    tr = sdpbresults['terminateReason']

    if tr in ['maxIterations exceeded', 'maxRuntime exceeded']:
        return (True, filename)
    if tr == 'maxComplementarity exceeded':
        return (False, None)
    # remaining terminateReasons that indicate 'successful' completion
    if tr in ['found primal-dual optimal solution',
              'found primal feasible solution',
              'found dual feasible solution',
              'primal feasible jump detected',
              'dual feasible jump detected']:
        return (True, None)
    return (False, None)


def nextinbinarysearchDecider(filename):
    """
    Creates the next file in a binary search.
    """
    sdpbresults = _getsdpbresults(filename)
    tr = sdpbresults['terminateReason']

    stream = open(filename, 'r')
    yaml = YAML()
    job = yaml.load(stream)
    bs = job['binarysearch']

    if (tr == 'found primal feasible solution' or
            tr == 'primal feasible jump detected'):
        _update_aliased_scalar(job, bs, 'primal', bs['cur'])
    elif (tr == 'found dual feasible solution' or
            tr == 'dual feasible jump detected'):
        _update_aliased_scalar(job, bs, 'dual', bs['cur'])
    else:
        raise ValueError('Cannot parse terminateReason for binary search.')

    cur = decimal.Decimal(bs['primal'])/2 + decimal.Decimal(bs['dual'])/2
    _update_aliased_scalar(job, bs, 'cur', str(cur))
    newfilename = _counteradd(filename)
    yaml.dump(job, open(newfilename, 'w'))

    # if we are done: create an otherwise empty yaml file with the result
    # and give it the 'terminated' status. The absence of an 'execute'
    # dictionary signifies to 'nextinsequencer' that there is no next job.
    diff = abs(decimal.Decimal(bs['primal']) - decimal.Decimal(bs['dual']))
    if diff < decimal.Decimal(bs['epsilon']):
        job = {'binarysearch': bs}
        newlog = jsonlogger.JSONlog('sdpanalyzer', newfilename + '.log')
        newlog.setstatus('terminated')
        return (True, None)
    else:
        return (True, newfilename)


def nextinsequencerDecider(filename):
    """
    Given a wrapper file for a sequence of jobs, move the pointer to the
    last unfinished file in the sequence. Then return the file for further
    execution.
    """
    stream = open(filename, 'r')
    yaml = YAML()
    job = yaml.load(stream)
    jobseq = job['sequencer']
    jobseqfilename = jobseq['startfile']

    while True:
        nextjobseqfilename = _counteradd(jobseqfilename)
        if not os.path.isfile(nextjobseqfilename):
            break
        else:
            jobseqfilename = nextjobseqfilename

    jobseqstream = open(jobseqfilename, 'r')
    jobseqjob = yaml.load(jobseqstream)
    # a sequence is completed if there is nothing to execute in the last
    # file of the sequence.
    if 'execute' not in jobseqjob:
        return (True, None)

    newfilename = _counteradd(filename)
    _update_aliased_scalar(job, jobseq, 'startfile', jobseqfilename)
    yaml.dump(job, open(newfilename, 'w'))
    return (True, newfilename)
