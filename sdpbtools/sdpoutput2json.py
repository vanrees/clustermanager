#!/usr/bin/env python3

"""
Locates the output file of sdpb and prints the results in standard dictionary
format. Can eat the same arguments as sdpb.
"""

import argparse
import os
import json

parser = argparse.ArgumentParser(description='Translate sdpb out.txt to dict.')
parser.add_argument('-s', '--sdpDir', metavar='arg', help='Directory containing sdp data files.')
parser.add_argument('-o', '--outDir', metavar='arg', help='Directory containing sdp output files.')
args, unknown = parser.parse_known_args()

# abspath removes trailing slashes
if args.outDir is None:
	args.outDir = os.path.abspath(args.sdpDir) + '_out'

outfile = os.path.abspath(args.outDir) + '/out.txt'

outdict = {}
for line in open(outfile,'r'):
	strippedline = line.strip(';\n')
	key, val = strippedline.split('=')
	outdict.update({key.strip() : val.strip('" ')})

print(json.dumps(outdict))