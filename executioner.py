#!/usr/bin/env python3

"""
Execute a series of programs in the 'execute' part of a yaml file which is in
turn specified as a command line argument. The idea is that every program
depends on the output of the previous program so we do not use parallelization.
Some minimal logging is done to the file with the same name plus a '.log'
extension. If desired stdout can be captured and written to the log.

This program can be customized to perform various checks.
"""

import argparse
import yaml
import os.path
import subprocess
import jsonlogger
import shlex

parser = argparse.ArgumentParser(description=("Execute a series of jobs."
                                              "Log the output and check exit"
                                              "status for each job"))
parser.add_argument('filename',
                    metavar='file',
                    help="""A yaml file with an 'execute' dictionary.""")
args = parser.parse_args()

logfilename = args.filename + '.log'
log = jsonlogger.JSONlog('executioner', logfilename)
log.setstatus('running')

# It's OK to fail hard if either of the below does not work.
stream = open(args.filename, 'r')
parse = yaml.safe_load(stream)

if 'execute' in parse:
    executions = parse['execute']
    for executable in executions:
        program = executable['program']

        # collect arguments to the program
        callargs = shlex.split(program)
        if 'cmdlineflags' in executable:
            # executable['cmdlineflags'] is a list of strings
            for flag in executable['cmdlineflags']:
                callargs += [flag]
        if 'cmdlineparams' in executable:
            # executable['cmdlineparams'] is a dict
            for key, value in executable['cmdlineparams'].items():
                callargs += ['--'+key+'='+value]
        # set other options
        runoptions = {}
        if 'options' in executable:
            runoptions.update(executable['options'])

        log.write({'starting': ' '.join(callargs)})

        # this works in in Python 3.7; capturing output works by adding
        # 'capture_output' : True
        # to the 'options' dictionary.

        # completedprocess = subprocess.run(callargs,text=True,**runoptions)

        # this works for Python 3.6 (and probably some earlier versions):
        # we remove 'capture_output' : ... and add the relevant
        # argument ourselves.
        parsedrunoptions = {}
        if 'capture_output' in runoptions:
            if runoptions['capture_output']:
                parsedrunoptions['stdout'] = subprocess.PIPE
                parsedrunoptions['encoding'] = 'ascii'
            del runoptions['capture_output']

        completedprocess = subprocess.run(callargs,
                                          **runoptions,
                                          **parsedrunoptions)
        completedprocess.check_returncode()
        programname = os.path.split(program)[1]
        # stdout is captured if capture_output=True is in 'options'
        log.write({'finished': programname, 'stdout': completedprocess.stdout})

log.setstatus('done')
