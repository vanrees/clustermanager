"""
A 'world' is supposed to define:
- submit(filename),  which submits the file to the cluster and returns a string
known as the submissionresult.
- check(submissionresult), which given the submissionresult returns True if
the job is really on the cluster (queued or running) and False otherwise.
"""

import subprocess


def submit(filename):
    """
    A function that submits a job to the cluster. It should return the
    result (typically a submissionid found in stdout - see below). This
    default implementation is OK for a desktop: it does not submit, rather
    it just runs the job.
    """
    print()
    print('===================== executing ===================')
    subprocess.run(['executioner.py', filename])
    print('===================== completed ===================')
    submissionresult = "Nothing was submitted"
    return submissionresult


def check(submissionresult):
    """
    A function that takes the submissionresult and verifies if the job is
    really running. This is the only function to use submissionresult. For
    a desktop machine we should not be doing this check until  after the
    job has completed - so the default is to return False.
    """
    return False
