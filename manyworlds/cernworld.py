"""
A 'world' is supposed to define:
- submit(filename),  which submits the file to the cluster and returns a string
known as the submissionresult.
- check(submissionresult), which given the submissionresult returns True if
the job is really on the cluster (queued or running) and False otherwise.
"""

import yaml
import htcondor as htc
import os
import subprocess


_bindir = "/afs/cern.ch/user/v/vanrees/.local/bin/"


def _hide(filename):
    path, file = os.path.split(filename)
    return os.path.join(path, '.' + file)


def submit(filename):
    """
    A 'submit' that works on the CERN cluster. Reads additional submission
    parameters from the file  itself.
    """
    fullfilename = os.path.abspath(filename)

    submissiondict = \
        {"executable": _bindir + "singularitywrapper.sh",
         "arguments": _bindir + "executioner.py " + fullfilename,
         "log": _hide(fullfilename + '.condorlog'),
         "output": _hide(fullfilename + '.out'),
         "error": _hide(fullfilename + '.err')}

    stream = open(filename, 'r')
    parse = yaml.safe_load(stream)
    if 'submit' in parse:
        submissiondict.update(parse['submit'])

        op = subprocess.check_output(
                ['condor_submit', '-terse'],
                input=str(htc.Submit(submissiondict)),
                encoding='utf-8')
        # expected output: XXXXX.0 - XXXXX.0 with XXXXX the submissionid
        return op.split()[0].split('.')[0]

    # alternative submission code below:
    # coll = htc.Collector()
    # schedd_ad = coll.locate(htc.DaemonTypes.Schedd)
    # self.schedd = htc.Schedd(schedd_ad)
    # with self.schedd.transaction() as txn:
    #     submissionid = str(sub.queue(txn))
    #     return submissionid
    # this alternaitve should work according to docs, but does not:
    # submitted jobs get held because log file cannot be created.
    # Interestingly, the problem disappears after using condor_submit once.
    # (perhaps an authentication error?)


def check(submissionresult):
    """
    Checks by querying the CERN cluster. Should not be used too often to avoid
    overloading the scheduler.
    """
    coll = htc.Collector()
    schedd = htc.Schedd(coll.locate(htc.DaemonTypes.Schedd))
    it = schedd.xquery(requirements='ClusterId =?= ' + submissionresult,
                       projection=['JobStatus'])
    # 0	Unexpanded
    # 1	Idle
    # 2	Running
    # 3	Removed
    # 4	Completed
    # 5	Held
    # 6	Submission_err
    for i in it:
        if i['JobStatus'] == 1 or i['JobStatus'] == 2:
            return True

    return False
